//
//  LoginViewControllerExtension.swift
//  BrightHR
//
//  Created by Grecu, Mircea on 16/01/2018.
//  Copyright © 2018 MG. All rights reserved.
//

import UIKit

extension LoginViewController: UITextFieldDelegate {
    
    func checkLoginButtonIsEnabled() {
        guard let username = usernameTextField.text,
            let password = passwordTextField.text else {
                loginButton.isEnabled = false
                return
        }
        
        if !username.isEmpty && !password.isEmpty {
            loginButton.isEnabled = true
        } else {
            loginButton.isEnabled = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        checkLoginButtonIsEnabled()
        return true
    }
}
