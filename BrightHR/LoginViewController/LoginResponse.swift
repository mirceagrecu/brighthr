//
//  LoginResponse.swift
//  BrightHR
//
//  Created by Grecu, Mircea on 14/01/2018.
//  Copyright © 2018 MG. All rights reserved.
//

import Foundation

/// Login network service response model
struct LoginResponse: Decodable {
    
    var companyId: Int
    var userId: Int
    var companyTimeZoneName: String
    var userTimeZoneName: String
    var token: Token
    var hasFixedWorkingTimePattern: Bool
}

struct Token: Decodable {
    var token: String
    var userId: String?
}
