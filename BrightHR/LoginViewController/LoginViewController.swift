//
//  ViewController.swift
//  BrightHR
//
//  Created by Grecu, Mircea on 14/01/2018.
//  Copyright © 2018 MG. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var loginButton: UIButton!
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    lazy var loginViewModel = LoginModelViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commonInit()
    }
    
    ///Common init for Login screen
    func commonInit() {
        passwordTextField.setBottomBorder()
        usernameTextField.setBottomBorder()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOnView))
        view.addGestureRecognizer(tapGesture)
        
        loginButton.setTitleColor(UIColor.lightGray, for: UIControlState.disabled)
    }

    ///Login button handler
    @IBAction func didTapLogin(sender: UIButton) {
        guard let username = usernameTextField.text,
              let password = passwordTextField.text,
              !username.isEmpty && !password.isEmpty else {
              checkLoginButtonIsEnabled()
              return
        }
        
        startSpinner()
        
        loginViewModel.fetchLoginDetails(with: username, and: password) { result in
            self.stopSpinner()
            
            switch result {
            case .success(let loginResponse):
                
                let loginMessage = "Welcome to \(loginResponse.companyTimeZoneName)"
                let alert = self.makeAlert(with: "Welcome", message: loginMessage)
                self.present(alert, animated: true, completion: nil)
                
            case .fail(let error):
                
                var errorMessage: String
                if let loginError = error as? LoginModelViewController.LoginError {
                    errorMessage = loginError.errorMessage()
                } else {
                    errorMessage = error.localizedDescription
                }
                
                let alert = self.makeAlert(with: "Error", message: errorMessage)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    ///Factory method for creating an alert controller
    func makeAlert(with title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { alertAction in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(cancelAction)
        
        return alert
    }
    
    func startSpinner() {
        loginButton.isEnabled = false
        spinner.startAnimating()
    }

    func stopSpinner() {
        loginButton.isEnabled = true
        spinner.stopAnimating()
    }
    
    @objc func didTapOnView() {
        checkLoginButtonIsEnabled()
        view.endEditing(true)
    }
}
