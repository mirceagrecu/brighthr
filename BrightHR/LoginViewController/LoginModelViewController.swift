//
//  LoginModelViewController.swift
//  BrightHR
//
//  Created by Grecu, Mircea on 14/01/2018.
//  Copyright © 2018 MG. All rights reserved.
//

import Foundation
import Alamofire

/// Login view controller model used to manage login operations
class  LoginModelViewController {
    
    private let loginURL = "https://brighthr-api-uat.azurewebsites.net/api/Account/PostValidateUser"
    
    /// Expose alamofire methods for testing purpose
    var makeRequest: ((URLConvertible, HTTPMethod, Parameters?, ParameterEncoding, HTTPHeaders?) -> DataRequest)? = Alamofire.request
    
    var responseJson: ((DataRequest) -> (DispatchQueue?, JSONSerialization.ReadingOptions, @escaping (DataResponse<Any>) -> Void) -> DataRequest)? = DataRequest.responseJSON
    
    public func fetchLoginDetails(with username: String, and password: String, completion: @escaping (Result<LoginResponse>) -> Void) {
        
        let httpHeaders: HTTPHeaders = ["Content-Type": "application/x-www-form-urlencoded"]
        let parameters = ["username": username,
                          "password": password]
        
        guard let url = URL(string: loginURL) else {
            completion(Result.fail(LoginError.invalidURL))
            return
        }
        
        guard let dataRequest = makeRequest?(url, HTTPMethod.post, parameters, URLEncoding.default, httpHeaders) else {
            completion(Result.fail(LoginError.unknownError))
            return
        }
        
        _ = responseJson?(dataRequest)(nil, .allowFragments) { response in
            if let error = response.error {
                completion(Result.fail(error))
                return
            }
            
            guard let statusCode = response.response?.statusCode else {
                completion(Result.fail(LoginError.unknownError))
                return
            }
            
            switch statusCode {
            case LoginStatusCodes.invalidCredentials.rawValue:
                completion(Result.fail(LoginError.invalidCredentials))
                return
                
            case LoginStatusCodes.successfulLogin.rawValue:
                guard let data = response.data,
                    let loginResponse = self.makeLoginResponseWith(data: data) else {
                        completion(Result.fail(LoginError.unknownError))
                        return
                }
                completion(Result.success(loginResponse))
                return
                
            default:
                completion(Result.fail(LoginError.unknownError))
                return
            }
        }
    }
    
    func makeLoginResponseWith(data: Data) -> LoginResponse? {
        guard let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: data) else {
            return nil
        }
        return loginResponse
    }
    
    enum Result<T> {
        case success(T)
        case fail(Error)
    }
    
    enum LoginError: Error {
        case invalidURL
        case invalidCredentials
        case unknownError
        
        func errorMessage() -> String {
            switch self {
            case .invalidCredentials:
                return "Please enter a valid username and password."
            case .invalidURL:
                return "Invalid URL!"
            default:
                return "Sorry, something has gone wrong. Please try again!"
            }
        }
    }
    
    enum LoginStatusCodes: Int {
        case successfulLogin = 200
        case invalidCredentials = 403
    }
    
}
