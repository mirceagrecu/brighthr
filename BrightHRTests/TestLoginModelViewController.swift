//
//  TestLoginModelViewController.swift
//  BrightHRTests
//
//  Created by Grecu, Mircea on 16/01/2018.
//  Copyright © 2018 MG. All rights reserved.
//

import XCTest
import Alamofire
@testable import BrightHR

class TestLoginModelViewController: XCTestCase {
    
    let model = LoginModelViewController()
    
    func testFetchSuccessfulData() {
        
        let expectationRequest = expectation(description: "Make Request!")
        
        model.makeRequest = { _, _, _, _, _ in
            
            expectationRequest.fulfill()
            return Alamofire.request(URL(string: "www.google.com")!)
        }
        
        let expectationResponseJSON = expectation(description: "Make response JSON")
        
        model.responseJson = { request in
            expectationResponseJSON.fulfill()
            return { _, _, completion in
                
                let response = self.mockHTTPResponse(with: 200)
                
                let dataResponse = DataResponse<Any>(request: nil, response: response, data: self.mockData(), result: Result.success(""))
                
                completion(dataResponse)
                
                return request
            }
        }
        
        model.fetchLoginDetails(with: "", and: "") { response in
            
            switch response {
            case .fail(_):
                XCTFail("Test fail!")
            case .success(let loginResponse):
                XCTAssertEqual(loginResponse.userId, 25050)
                XCTAssertEqual(loginResponse.companyId, 22846)
                XCTAssertEqual(loginResponse.hasFixedWorkingTimePattern, true)
            }
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testFetchFailData() {
        
        let expectationRequest = expectation(description: "Make Request!")
        
        model.makeRequest = { _, _, _, _, _ in
            
            expectationRequest.fulfill()
            return Alamofire.request(URL(string: "www.google.com")!)
        }
        
        let expectationResponseJSON = expectation(description: "Make response JSON")
        
        model.responseJson = { request in
            expectationResponseJSON.fulfill()
            return { _, _, completion in
                
                let response = self.mockHTTPResponse(with: 403)
                
                let dataResponse = DataResponse<Any>(request: nil, response: response, data: self.mockData(), result: Result.success(""))
                
                completion(dataResponse)
                
                return request
            }
        }
        
        model.fetchLoginDetails(with: "", and: "") { response in
            
            switch response {
            case .fail(let error):
                
                guard let loginError = error as? LoginModelViewController.LoginError else {
                    XCTFail("Test fail!")
                    return
                }
                
                XCTAssertEqual(loginError.errorMessage(), "Please enter a valid username and password.")
            case .success(_):
                XCTFail("Test fail!")
                return
            }
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testObjectJSON() {
        let jsonPath = Bundle.main.path(forResource: "LoginResponse", ofType: "json")
        guard let filepath = jsonPath,
              let contentOfFile = try? Data(referencing: NSData(contentsOfFile: filepath)) else {
                XCTFail("Test Fail!")
                return
        }
        
        guard let loginResponse = model.makeLoginResponseWith(data: contentOfFile) else {
            XCTFail("Test Fail Invalid JSON!")
            return
        }
        
        XCTAssertEqual(loginResponse.token.token, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjI1MDUwLCJDb21wYW55SWQiOjIyODQ2LCJFeHBpcnlEYXRlIjoiXC9EYXRlKDE1MDc3Njc2ODE4ODkpXC8ifQ.PNM143ErUvcEfxr0c9bS0vD_wMmMrpYdVkk0YLpHLOk")
        XCTAssertNil(loginResponse.token.userId)
        XCTAssertEqual(loginResponse.userId, 25050)
        XCTAssertEqual(loginResponse.companyId, 22846)
        XCTAssertEqual(loginResponse.hasFixedWorkingTimePattern, true)
        XCTAssertEqual(loginResponse.userTimeZoneName, "Europe/London")
        XCTAssertEqual(loginResponse.companyTimeZoneName, "Europe/London")
    }
    
    
    
}

extension TestLoginModelViewController {
    
    func mockLoginResponse() -> LoginResponse? {
        let jsonPath = Bundle.main.path(forResource: "LoginResponse", ofType: "json")
        guard let filepath = jsonPath,
            let contentOfFile = try? Data(referencing: NSData(contentsOfFile: filepath)),
            let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: contentOfFile) else {
                return nil
        }
        return loginResponse
    }
    
    func mockData() -> Data? {
        let jsonPath = Bundle.main.path(forResource: "LoginResponse", ofType: "json")
        guard let filepath = jsonPath,
            let contentOfFile = try? Data(referencing: NSData(contentsOfFile: filepath)) else {
                return nil
        }
        return contentOfFile
    }
    
    func mockHTTPResponse(with code: Int) -> HTTPURLResponse? {
        return HTTPURLResponse(url: URL(string: "www.google.com")!, statusCode: code, httpVersion: nil, headerFields: nil)
    }
}
